import numpy as np
from PIL import Image

from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import load_img, img_to_array

def get_train_datagenerator(normalize_pixel_values):
    return ImageDataGenerator(
                rescale=_get_rescaling(normalize_pixel_values),
                shear_range=0.2,
                zoom_range=0.2,
                horizontal_flip=True)

def get_test_data_generator(normalize_pixel_values):
    return ImageDataGenerator(rescale=_get_rescaling(normalize_pixel_values))

def get_img_array_from_path(path, normalize_pixel_values, target_size):
    img = Image.open(path)
    img = img.resize(size=target_size)
    array_img = np.asarray(img, dtype=np.float32) * _get_rescaling(normalize_pixel_values)
    return np.resize(array_img, [1, *array_img.shape])

def _get_rescaling(normalize_pixel_values):
    rescale = 1
    if normalize_pixel_values:
        rescale = 1./255
    return rescale
