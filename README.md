# Setting up dvc

For our data and model versioning we use the tool [dvc](https://dvc.org/). 
An introduction to dvc be found, e.g., [here](https://blog.codecentric.de/en/2019/03/walkthrough-dvc/).
Using the setup requires to setup a working dvc project with an S3 bucket as [remote storage](https://dvc.org/doc/command-reference/remote/add).

For our toy problem we use a subset of 11 kinds of fruit from the [Fruit360 dataset](https://github.com/Horea94/Fruit-Images-Dataset).
In order to run the example you need to copy a subset of the fruit folders in the folder
`data/fruits/test` and `data/fruits/training`, respectively. 
Then, add the `data` folder to your dvc project and push the data into the remote storage.


# Setting up the EC2 Instance for model training

Before the pipeline can be used, we have to setup a suitable machine for the training of the model. 
In our example we use a t2.medium ec2-instance with Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type - ami-010fae13a16763bb4. 

The AMI already includes docker, however, we have to provision additional software. 
In particular, we require git and dvc. 

Furthermore, we have to setup a ssh key pair to read and write the Git repository containing the ML pipeline.
Also, we have to manually clone the repository to the ec2-instance.

Finally, we want the gitlab runner to orchestrate the training on the ec2-instance.
For this purpose, we use the aws system manager, which allows to control an ec2-instance remotely.
Therefore, we have to attach a suitable IAM Instance Profile to the ec2-instance. 
This can be done via the following [user guide](https://docs.aws.amazon.com/en_pv/systems-manager/latest/userguide/systems-manager-setting-up.html).

```
# install git
sudo yum update -y
sudo yum install git -y

# generate ssh key pair
# (if you setup more than one ec2 instance, reuse the key generated for the first one)
ssh-keygen -o -f ~/.ssh/id_rsa

# add key pair to gitlab, such that this instance can read/write the repository

# clone repo
git clone git@gitlab.com:bbesser/dvc-remote-training.git

# install dvc 
sudo wget https://dvc.org/rpm/dvc.repo -O /etc/yum.repos.d/dvc.repo
sudo yum update -y
sudo yum install dvc -y

# add s3 credentials to instance (files ~/.aws/config and ~/.aws/credentials)
```

# Setup CI/CD Environment Variables

The following variables are used in the ci pipeline

```
AWS_ACCESS_KEY_ID = <my_aws_access_key_id>
AWS_DEFAULT_REGION = <my_aws_default_region>
AWS_SECRET_ACCESS_KEY = <my_aws_secret_access_key>
BUCKET_NAME = <my_aws_s3_bucket_name>
DOCKER_REGISTRY = <my_docker_registry_name>
DOCKER_TAG = <my_docker_tag>

GITLAB_TOKEN = <my_gitlab_access_token>

TRAIN_INSTANCE_FOR_USER = {"dev1@mail.com": <EC2-ID-1>, "dev2@mail.com": <EC2-ID-1> }
```

# Building the Docker image
To trigger the Docker build (on any branch):
```
git commit --allow-empty -m 'build image'
git push
```
The image will be pushed to the Amazon ECR under the tag `train`.

# Training
To trigger a training (on any branch):
```
git commit --allow-empty -m 'trigger training <tag_name>'
git push
```
where <tag_name> must match the regex `[a-zA-Z0-9_\-\.]+`.
The tag_name will be used to tag the dvc-project after training.
It will also be used as the release name of the new `train.dvc`.  

# Building the training image
To build the training image (on any branch):
```
git commit --allow-empty -m 'build image'
git push
```
The branch name will be part of the docker registry tag for the image.
(tag: train-<branch name>)
