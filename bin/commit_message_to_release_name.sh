#!/usr/bin/env sh
# the format of a commit message triggering a training is:
# trigger training <tag_name>
# this script extracts the third token

training_trigger_commit_message="$1"
tag_name=`echo "$training_trigger_commit_message" | cut -d' ' -f3`
echo $tag_name
