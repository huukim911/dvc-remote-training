import fire
import boto3
import time
import json

from execute_bash_on_ec2 import execute_command, check_if_accessible

ec2_client = boto3.client('ec2')
ec2 = boto3.resource('ec2')

def execute_orchestration(project_name, ec2_id_file, user_name, branch_name, docker_registry, docker_tag, release_name):

    # Get EC2 Instance ID for user
    ec2_id = _get_instanceID_by_user(ec2_id_file, user_name)

    # Start EC2 Instance and wait until started
    print("--- starting ec2 instance ----")
    _start_instance(ec2_id)

    # git pull and dvc pull latest version from git repo to ec2-instance
    print("--- updateing ec2 instance ----")
    cmd = _get_update_command(project_name, branch_name)
    execute_command(ec2_id, cmd)

    # Execute train_model.sh on ec2 instance
    print("--- train model ---")
    cmd = _get_train_command(project_name, docker_registry, docker_tag, release_name)
    execute_command(ec2_id, cmd)

    # Stop EC2 Instance and wait until stopped
    print("--- stopping ec2 instance ----")
    _stop_instance(ec2_id)


def _start_instance(ec2_id):
    ec2_client.start_instances(
        InstanceIds=[ec2_id],
        DryRun=False
    )
    ec2.Instance(ec2_id).wait_until_running()
    check_if_accessible(ec2_id)
    print("started and available")

def _stop_instance(ec2_id):
    ec2_client.stop_instances(
        InstanceIds=[ec2_id],
        DryRun=False
    )
    ec2.Instance(ec2_id).wait_until_stopped()
    print("stopped")

def _get_update_command(project_name, branch_name):
    print("branch name = ", branch_name)
    return f"su - ec2-user -c \"cd {project_name} && git fetch && git checkout {branch_name} && git pull && dvc pull -a -T\""

def _get_train_command(project_name, docker_registry, docker_tag, release_name):
    print("docker registry = ",  docker_registry)
    print("docker tag = ", docker_tag)
    print("commit_name = ", release_name)

    return f"su - ec2-user -c \"cd {project_name} && ./bin/train_model.sh '{docker_registry}:{docker_tag}' '{release_name}'\""

def _get_instanceID_by_user(ec2_id_file, user_name):
    with open(ec2_id_file, 'r') as f:
        ec2_ids = json.load(f)

    return ec2_ids[user_name]

if __name__ == '__main__':
    fire.Fire()


