#!/usr/bin/env bash

DOCKER_IMAGE="$1"
RELEASE_NAME="$2"

### get docker image for training
eval $(aws ecr get-login --region eu-central-1 --no-include-email)
docker pull $DOCKER_IMAGE

### run training with docker container
docker run -v .:/app $DOCKER_IMAGE dvc repro train.dvc

### add resulting train.dvc to git and push artifacts to dvc
git add train.dvc
git commit --allow-empty -m "ci-commit: $RELEASE_NAME"
git push

git tag -m "$RELEASE_NAME" $RELEASE_NAME
git push origin $RELEASE_NAME

dvc push -a -T
